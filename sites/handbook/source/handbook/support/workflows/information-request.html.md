---
layout: handbook-page-toc
title: Subpoenas, Court Orders and other requests for user information
category: Legal
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview
There may be times when a request comes in that seems to need input from our Legal team. This workflow explains how to handle such a request when it does not fit into one of the prescribed workflows.

## Workflows

### Incoming Subpoena or court order

From time to time we may receive subpoenas or other legal requests for information about GitLab users, their data or activity.
This workflow clarifies how to handle these requests and subsequent workflow related to delivering information if
our counsel and CISO approve.

*Note:* If a ticket comes in that you believe is Legal related, but is not a subpoena or court order, please check the list of other [legal workflows](https://about.gitlab.com/handbook/support/workflows/#Legal).

If a subpoena or court order that is not covered by another workflow (e.g. DMCA, GDPR, information request) comes in:

1. Forward the email to legal@gitlab.com
1. Reach out on the Slack #legal channel to inform the Legal team.
1. Respond to the ticket:

```
Your request has been received and forwarded to our legal department who will review this request.

Please direct any follow-up or additional queries of this nature directly to the legal team by using the email address legal@gitlab.com.

This ticket will be marked as "Solved"
```

### GitLab.com specific support policies

These workflows should be used when a user question is addressed in a [specific Support policy](https://about.gitlab.com/support/#gitlabcom-specific-support-policies).

#### Namespace and Trademark claims 

Respond to the ticket:

```
Your request has been received. GitLab does not take action on trademark or other namespace disputes without proper legal orders. For more information, see our [Namespace and Trademark policy](https://about.gitlab.com/support/#namespace--trademarks).

This ticket will be marked as "Solved"
```

#### Ownership Disputes 
*Note: This workflow is specifically related to inquiries about disputes on ownership. You can also check other [ownership-related workflows](https://about.gitlab.com/handbook/support/workflows/account_verification.html) to ensure you are using the correct one.*

Respond to the ticket:

```
Your request has been received. GitLab does not take action on ownership disputes without proper legal orders. For more information, see our [Ownership Disputes policy](https://about.gitlab.com/support/#ownership-disputes).

This ticket will be marked as "Solved"
```

#### Log Requests 

Please see [Log requests workflow](https://about.gitlab.com/handbook/support/workflows/log_requests.html)

### Other Legal-related Questions
A request may come in that you believe needs legal input but does not fit into one of the prescribed workflows. Follow this workflow in such a situation:

1. Follow the instructions for [contacting Legal](https://about.gitlab.com/handbook/legal/#contacting-the-legal-team-with-general-legal-questions) based on the specifics of the request.
1. Respond to the ticket:

```
Your request has been received and forwarded to our legal department who will review this request.

When we have received a response from our Legal team, this ticket will be updated.
```

1. Put ticket on hold. 
1. If ticket has not been resolved within 4 days (when ticket will auto-resurrect), check in with Legal for an answer. 
1. If Legal requests more time, respond to the ticket:

```
Since this is a complex request, we're going to hand over this issue directly to the Legal team. Once they have more information for you, you can expect an email directly from them. 

We're tracking this internally in <link-to-legal-and-compliance-issue>. Note: you won't be able to see this internal request, but it will be helpful to reference if you don't hear anything back within time period X

Since we're handing off directly to the legal team we'll solve this support request to make sure we're using just one form of communication at a time. If you need to follow up before hearing from them, please email Legal directly at legal@gitlab.com.
```
1. Note the above response in the related Legal and Compliance issue.
1. Close ticket.

### Unsure? Other?

Support should be handling responses to any inquiries for clarifications. In most cases, an existing customer will be referred to their account manager.

If you're unsure on how to respond, post in the #support_managers Slack channel for guidance.
