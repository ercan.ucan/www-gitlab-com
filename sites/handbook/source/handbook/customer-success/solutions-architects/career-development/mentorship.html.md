---
layout: handbook-page-toc
title: "Solution Architect Mentorship Program (SAMP)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Goals

The Solution Architect Mentorship Program (SAMP) supports GitLab Values of Collaboration and Diversity, Inclusion, and Belonging by purposefully creating and cultivating opportunities for mentors and mentees to engage over critical questions in the organization and in their professional lives.

The goals of the Solution Architect Mentorship Program are to
- Provide informal professional and career guidance to Solution Architects
- Assist team members in meeting their career goals by honing new capabilities
- Build strong, lasting relationships between team members and across GitLab
- Improve GitLab’s ability to attract, retain and engage new team members from diversity, inclusion, and belonging groups

If successful, we will consider an expansion to other groups across the company. In the spirit of iteration, we would like to get started soon and expand and improve as we go.

<!-- Specify that it’s 30-minute coaching sessions. Opening network opportunities. Public speaking, leadership, shadowing.-->
